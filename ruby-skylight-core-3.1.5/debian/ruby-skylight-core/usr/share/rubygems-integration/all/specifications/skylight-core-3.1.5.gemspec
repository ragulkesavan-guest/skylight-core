# -*- encoding: utf-8 -*-
# stub: skylight-core 3.1.5 ruby lib

Gem::Specification.new do |s|
  s.name = "skylight-core".freeze
  s.version = "3.1.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Tilde, Inc.".freeze]
  s.date = "2019-03-31"
  s.email = ["engineering@tilde.io".freeze]
  s.files = ["lib/skylight/core.rb".freeze, "lib/skylight/core/config.rb".freeze, "lib/skylight/core/deprecation.rb".freeze, "lib/skylight/core/errors.rb".freeze, "lib/skylight/core/fanout.rb".freeze, "lib/skylight/core/formatters/http.rb".freeze, "lib/skylight/core/gc.rb".freeze, "lib/skylight/core/instrumentable.rb".freeze, "lib/skylight/core/instrumenter.rb".freeze, "lib/skylight/core/middleware.rb".freeze, "lib/skylight/core/normalizers.rb".freeze, "lib/skylight/core/normalizers/action_controller/process_action.rb".freeze, "lib/skylight/core/normalizers/action_controller/send_file.rb".freeze, "lib/skylight/core/normalizers/action_view/render_collection.rb".freeze, "lib/skylight/core/normalizers/action_view/render_partial.rb".freeze, "lib/skylight/core/normalizers/action_view/render_template.rb".freeze, "lib/skylight/core/normalizers/active_job/perform.rb".freeze, "lib/skylight/core/normalizers/active_model_serializers/render.rb".freeze, "lib/skylight/core/normalizers/active_record/instantiation.rb".freeze, "lib/skylight/core/normalizers/active_record/sql.rb".freeze, "lib/skylight/core/normalizers/active_support/cache.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_clear.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_decrement.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_delete.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_exist.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_fetch_hit.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_generate.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_increment.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_read.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_read_multi.rb".freeze, "lib/skylight/core/normalizers/active_support/cache_write.rb".freeze, "lib/skylight/core/normalizers/coach/handler_finish.rb".freeze, "lib/skylight/core/normalizers/coach/middleware_finish.rb".freeze, "lib/skylight/core/normalizers/data_mapper/sql.rb".freeze, "lib/skylight/core/normalizers/default.rb".freeze, "lib/skylight/core/normalizers/elasticsearch/request.rb".freeze, "lib/skylight/core/normalizers/faraday/request.rb".freeze, "lib/skylight/core/normalizers/grape/endpoint.rb".freeze, "lib/skylight/core/normalizers/grape/endpoint_render.rb".freeze, "lib/skylight/core/normalizers/grape/endpoint_run.rb".freeze, "lib/skylight/core/normalizers/grape/endpoint_run_filters.rb".freeze, "lib/skylight/core/normalizers/grape/format_response.rb".freeze, "lib/skylight/core/normalizers/moped/query.rb".freeze, "lib/skylight/core/normalizers/render.rb".freeze, "lib/skylight/core/normalizers/sequel/sql.rb".freeze, "lib/skylight/core/normalizers/sql.rb".freeze, "lib/skylight/core/probes.rb".freeze, "lib/skylight/core/probes/action_controller.rb".freeze, "lib/skylight/core/probes/action_dispatch.rb".freeze, "lib/skylight/core/probes/action_dispatch/request_id.rb".freeze, "lib/skylight/core/probes/action_dispatch/routing/route_set.rb".freeze, "lib/skylight/core/probes/action_view.rb".freeze, "lib/skylight/core/probes/active_job.rb".freeze, "lib/skylight/core/probes/active_job_enqueue.rb".freeze, "lib/skylight/core/probes/active_model_serializers.rb".freeze, "lib/skylight/core/probes/elasticsearch.rb".freeze, "lib/skylight/core/probes/excon.rb".freeze, "lib/skylight/core/probes/excon/middleware.rb".freeze, "lib/skylight/core/probes/faraday.rb".freeze, "lib/skylight/core/probes/grape.rb".freeze, "lib/skylight/core/probes/httpclient.rb".freeze, "lib/skylight/core/probes/middleware.rb".freeze, "lib/skylight/core/probes/mongo.rb".freeze, "lib/skylight/core/probes/mongoid.rb".freeze, "lib/skylight/core/probes/moped.rb".freeze, "lib/skylight/core/probes/net_http.rb".freeze, "lib/skylight/core/probes/redis.rb".freeze, "lib/skylight/core/probes/sequel.rb".freeze, "lib/skylight/core/probes/sinatra.rb".freeze, "lib/skylight/core/probes/tilt.rb".freeze, "lib/skylight/core/railtie.rb".freeze, "lib/skylight/core/sidekiq.rb".freeze, "lib/skylight/core/subscriber.rb".freeze, "lib/skylight/core/test.rb".freeze, "lib/skylight/core/trace.rb".freeze, "lib/skylight/core/user_config.rb".freeze, "lib/skylight/core/util.rb".freeze, "lib/skylight/core/util/allocation_free.rb".freeze, "lib/skylight/core/util/clock.rb".freeze, "lib/skylight/core/util/gzip.rb".freeze, "lib/skylight/core/util/inflector.rb".freeze, "lib/skylight/core/util/logging.rb".freeze, "lib/skylight/core/util/platform.rb".freeze, "lib/skylight/core/util/proxy.rb".freeze, "lib/skylight/core/vendor/active_support/notifications.rb".freeze, "lib/skylight/core/vendor/active_support/per_thread_registry.rb".freeze, "lib/skylight/core/vendor/thread_safe.rb".freeze, "lib/skylight/core/vendor/thread_safe/non_concurrent_cache_backend.rb".freeze, "lib/skylight/core/vendor/thread_safe/synchronized_cache_backend.rb".freeze, "lib/skylight/core/version.rb".freeze, "lib/skylight/core/vm/gc.rb".freeze]
  s.homepage = "https://www.skylight.io".freeze
  s.licenses = ["Nonstandard".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.2.7".freeze)
  s.rubygems_version = "2.7.6".freeze
  s.summary = "The core methods of the Skylight profiler.".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activesupport>.freeze, [">= 4.2.0"])
      s.add_development_dependency(%q<beefcake>.freeze, ["< 1.0"])
      s.add_development_dependency(%q<bundler>.freeze, ["~> 1.15"])
      s.add_development_dependency(%q<puma>.freeze, [">= 0"])
      s.add_development_dependency(%q<rack>.freeze, [">= 0"])
      s.add_development_dependency(%q<rack-test>.freeze, [">= 0"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.7"])
      s.add_development_dependency(%q<rspec-collection_matchers>.freeze, ["~> 1.1"])
      s.add_development_dependency(%q<webmock>.freeze, [">= 0"])
    else
      s.add_dependency(%q<activesupport>.freeze, [">= 4.2.0"])
      s.add_dependency(%q<beefcake>.freeze, ["< 1.0"])
      s.add_dependency(%q<bundler>.freeze, ["~> 1.15"])
      s.add_dependency(%q<puma>.freeze, [">= 0"])
      s.add_dependency(%q<rack>.freeze, [">= 0"])
      s.add_dependency(%q<rack-test>.freeze, [">= 0"])
      s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_dependency(%q<rspec>.freeze, ["~> 3.7"])
      s.add_dependency(%q<rspec-collection_matchers>.freeze, ["~> 1.1"])
      s.add_dependency(%q<webmock>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<activesupport>.freeze, [">= 4.2.0"])
    s.add_dependency(%q<beefcake>.freeze, ["< 1.0"])
    s.add_dependency(%q<bundler>.freeze, ["~> 1.15"])
    s.add_dependency(%q<puma>.freeze, [">= 0"])
    s.add_dependency(%q<rack>.freeze, [">= 0"])
    s.add_dependency(%q<rack-test>.freeze, [">= 0"])
    s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.7"])
    s.add_dependency(%q<rspec-collection_matchers>.freeze, ["~> 1.1"])
    s.add_dependency(%q<webmock>.freeze, [">= 0"])
  end
end
