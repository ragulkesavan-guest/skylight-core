Format: 3.0 (quilt)
Source: ruby-skylight-core
Binary: ruby-skylight-core
Architecture: all
Version: 3.1.5-1
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: ragul and kailash <srk7official1998@gmail.com>
Homepage: https://www.skylight.io
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-skylight-core
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-skylight-core.git
Testsuite: autopkgtest-pkg-ruby
Build-Depends: debhelper (>= 11~), gem2deb, ruby-activesupport (>= 4.2.0)
Package-List:
 ruby-skylight-core deb ruby optional arch=all
Checksums-Sha1:
 c4e555c842282c68df9d729fe89c3ccfc25fe0b1 41131 ruby-skylight-core_3.1.5.orig.tar.gz
 f38bf8b28e53bceafd0eba5189b5bc71eaebc508 1888 ruby-skylight-core_3.1.5-1.debian.tar.xz
Checksums-Sha256:
 f1d2debeadc8dd39ee139664acfb3bfa1c74793246f824ff569aa6fd35d9a066 41131 ruby-skylight-core_3.1.5.orig.tar.gz
 65de1c11f09d4fd8bc14cc6e3da8b2fc14a199b40620f60958ad2554a5bc6b6b 1888 ruby-skylight-core_3.1.5-1.debian.tar.xz
Files:
 24cd9b1b2e08be7f97bd72124ac71d72 41131 ruby-skylight-core_3.1.5.orig.tar.gz
 503873018e1f4b38abefd9f58237936f 1888 ruby-skylight-core_3.1.5-1.debian.tar.xz
Ruby-Versions: all
